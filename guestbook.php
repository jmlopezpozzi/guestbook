<?php


function guestbook_sign($db, $name, $message)
{
	$name = trim($name);
	if (!$name) {
		return;
	}
	$name = htmlspecialchars($name);
	$name = strtoupper($name);
	$stmt = $db->prepare("SELECT id FROM visitor WHERE name LIKE :name");
	$stmt->execute(["name" => $name]);
	$visitor = $stmt->fetch();
	if ($visitor) {
		// Name has previous messages
		$visitor = $visitor["id"];
	}
	else {
		// First message with this name
		$new_visitor = $db->prepare(
			"INSERT INTO visitor (name) VALUES (:name)"
		);
		$new_visitor->execute(["name" => $name]);
		$visitor = $db->lastInsertId();
	}
	$new_message = $db->prepare(
		"INSERT INTO message (visitor_id, body) VALUES (:id, :body)"
	);
	$new_message->execute(["id" => $visitor, "body" => $message]);
}


function guestbook_read($db, int $entries_per_page = NULL, int $page = NULL)
{
	$base_query =
		"SELECT name AS visitor_name, time, body AS message_body
		FROM visitor, message
		WHERE message.visitor_id = visitor.id
		ORDER BY message.time DESC";
	$page_query =
		" LIMIT :num_entries OFFSET :offset";
	$entries = [];
	if ($entries_per_page == NULL || $entries_per_page < 1) {
		// Show all
		$stmt = $db->query($base_query);
	}
	else {
		if ($page == NULL || $page < 1) {
			$page = 1;
		}
		$stmt = $db->prepare($base_query . $page_query);
		$stmt->bindValue(":num_entries", $entries_per_page, PDO::PARAM_INT);
		$stmt->bindValue(":offset", ($page - 1) * $entries_per_page,
		                 PDO::PARAM_INT);
		$stmt->execute();
	}
	foreach ($stmt as $row) {
		$entries[] = $row;
	}
	return $entries;
}


function guestbook_read_signed_as($db, $name, int $entries_per_page = NULL,
                                  int $page = NULL)
{
	$name = trim($name);
	if (!$name) {
		return;
	}
	$name = htmlspecialchars($name);
	$name = strtoupper($name);
	$stmt = $db->prepare("SELECT id, name FROM visitor WHERE name LIKE :name");
	$stmt->execute(["name" => $name]);
	$visitor = $stmt->fetch();
	if (!$visitor) {
		return;
	}
	$base_query =
		"SELECT body AS message_body, time
		FROM message
		WHERE visitor_id = :id
		ORDER BY message.time DESC";
	$page_query =
		" LIMIT :num_entries OFFSET :offset";
	if ($entries_per_page == NULL || $entries_per_page < 1) {
		// Show all
		$stmt = $db->prepare($base_query);
		$stmt->execute(["id" => $visitor["id"]]);
	}
	else {
		if ($page == NULL || $page < 1) {
			$page = 1;
		}
		$stmt = $db->prepare($base_query . $page_query);
		$stmt->bindValue(":id", $visitor["id"]);
		$stmt->bindValue(":num_entries", $entries_per_page, PDO::PARAM_INT);
		$stmt->bindValue(":offset", ($page - 1) * $entries_per_page,
		                 PDO::PARAM_INT);
		$stmt->execute();
	}
	foreach ($stmt as $row) {
		$entries[] = $row;
	}
	$entries["visitor"] = $visitor;
	return $entries;
}


function guestbook_get_total_pages($db, int $entries_per_page,
                                   int $visitor_id = NULL)
{
	if ($entries_per_page < 1) {
		throw new OutOfRangeException(
			__FUNCTION__ . "(): entries_per_page must be higher than 0"
		);
	}
	if ($visitor_id) {
		$stmt = $db->prepare(
			"SELECT COUNT(*)
			FROM message
			WHERE visitor_id = :id"
		);
		$stmt->execute(["id" => $visitor_id]);
		$num_entries = $stmt->fetchColumn();
	}
	else {
		$num_entries = $db->query("SELECT COUNT(*) FROM message")->fetchColumn();
	}
	if (!$num_entries) {
		return 0;
	}
	$total_pages = ceil($num_entries / $entries_per_page);
	return $total_pages;
}
