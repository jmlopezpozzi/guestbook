<?php


function connect_db($settings_file, $pdo_options = NULL)
{
	if ($pdo_options == NULL) {
		$pdo_options = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES => false,
		];
	}
	$settings = parse_ini_file($settings_file);
	$dsn = "mysql:
		host={$settings['host']};
		port={$settings['port']};
		dbname={$settings['dbname']};
		charset={$settings['charset']}";
	try {
		$db = new PDO($dsn, $settings["user"], $settings["pw"], $pdo_options);
	}
	catch (PDOException $e) {
		throw new PDOException($e->getMessage(), (int) $e->getCode());
	}
	return $db;
}


/**
 * The $settings_file must be an INI file with the following keys:
 *
 * host, defining the database host;
 * port, defining the port used by the database on the host;
 * dbname, defining the name of the database in MySQL to use;
 * charset, defining the character encoding to use with the database;
 * user, defining the name of the database user;
 * pw, defining the corresponding password for user;
 *
 * These keys may have empty values but the keys themselves must be in
 * the file.
 */
