<?php

/**
 * This script is provided as an utility to initialize a database with
 * the proper structure for the guestbook program to use. It should
 * ideally not be exposed to the public. The database can alternatively
 * be initialized by hand by inputting the SQL statements below in the
 * MySQL shell, in which case this script can be prescinded entirely.
 */


require_once("db.php");


const SETTINGS_FILE = "settings.ini";

$db = connect_db(SETTINGS_FILE);

$stmt = $db->prepare(
	"CREATE TABLE IF NOT EXISTS visitor (
		id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		name VARCHAR(64) UNIQUE NOT NULL
	);"
);
$stmt->execute();

$stmt = $db->prepare(
	"CREATE TABLE IF NOT EXISTS message (
		id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		visitor_id INT UNSIGNED NOT NULL,
		time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
		body VARCHAR(255),
		FOREIGN KEY (visitor_id) REFERENCES visitor (id) ON UPDATE CASCADE
	);"
);
$stmt->execute();

echo "Database initialized\n";

?>
