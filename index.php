<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<style>
		.guestbook-message {
			margin: 1rem auto;
		}
	</style>
</head>
<body>
	<form action="index.php" method="post">
		<label for="visitor-name">Name:</label>
		<input type="text" id="visitor-name" name="visitor-name">
		<br>
		<label for="message">Message:</label>
		<textarea id="message" name="message"></textarea>
		<br>
		<button type="submit">SEND</button>
	</form>


<?php

require_once("db.php");
require_once("guestbook.php");


const SETTINGS_FILE = "settings.ini";
const ENTRIES_PER_PAGE = 10;
const PAGE_NAVIGATION_WIDGET_RANGE_MAX = 7;


$db = connect_db(SETTINGS_FILE);
if (isset($_POST["visitor-name"])) {
	try {
		guestbook_sign($db, $_POST["visitor-name"], $_POST["message"]);
	}
	catch (PDOException $e) {
		if ($e->getCode() == 22001) {
			echo "<div>Name or message too long</div>";
		}
		else {
			throw $e;
		}
	}
}
$page = isset($_GET["page"]) && is_numeric($_GET["page"]) ? $_GET["page"] : 1;
$visitor_id = NULL;
if (isset($_GET["by"])) {
	$entries_by = guestbook_read_signed_as($db, $_GET["by"], ENTRIES_PER_PAGE,
	                                       $page);
	$visitor_id = $entries_by["visitor"]["id"];
	$visitor_name = $entries_by["visitor"]["name"];
	unset($entries_by["visitor"]);
	show_entries_by($entries_by, $visitor_name);
}
else {
	$entries = guestbook_read($db, ENTRIES_PER_PAGE, $page);
	show_entries($entries);
}
echo page_navigation_widget($db, ENTRIES_PER_PAGE, $page,
                            PAGE_NAVIGATION_WIDGET_RANGE_MAX, $visitor_id);


function show_entries($entries)
{
	foreach ($entries as $entry) {
		echo "
			<div class=\"guestbook-message\">
				<strong>
					<a href=\"?by={$entry['visitor_name']}\">{$entry['visitor_name']}</a>
				</strong> on {$entry['time']}:<br>
				{$entry['message_body']}
			</div>
		";
	}
}


function show_entries_by($entries, $visitor_name)
{
	echo "<div><a href=\"index.php\">All visitors</a></div>";
	foreach ($entries as $entry) {
		echo "
			<div class=\"guestbook-message\">
				<strong>$visitor_name</strong> on {$entry['time']}:<br>
				{$entry['message_body']}
			</div>
		";
	}
}


function page_link(int $page)
{
	if (isset($_GET['page'])) {
		return "?" . preg_replace("/page=(\d)*/i", "page=$page",
		                          $_SERVER['QUERY_STRING']);
	}
	if ($_SERVER['QUERY_STRING'] != "") {
		return "?" . $_SERVER['QUERY_STRING'] . "&page=$page";
	}
	return "?page=$page";
}


function page_navigation_widget($db, $entries_per_page, $page, $range_max,
                                $visitor_id = NULL)
{
	try {
		$total_pages = guestbook_get_total_pages($db, $entries_per_page,
		                                         $visitor_id);
	}
	catch (OutOfRangeException $e) {
		return $e->getMessage();
	}
	if ($total_pages < 2) {
		return;
	}
	$range = $total_pages < $range_max ? $total_pages : $range_max;
	$range_half = floor($range / 2);
	$range_start = $page - $range_half;
	if ($range_start < 1) {
		$range_start = 1;
	}
	else
	if ($page >= $total_pages - $range_half + 1) {
		$range_start = $total_pages - $range + 1;
	}
	$labels = [
		"first" => "<< First",
		"previous" => "< Previous",
		"next" => "Next >",
		"last" => "Last >>"
	];
	$html = "";
	$html .= "<div>";
	if ($page > 1) {
		$link_first = page_link(1);
		$link_previous = page_link($page - 1);
		$html .= "<a href=\"$link_first\">{$labels['first']}</a>&#32;";
		$html .= "<a href=\"$link_previous\">{$labels['previous']}</a>&#32;";
	}
	for ($i = $range_start; $i < $range_start + $range; $i += 1) {
		if ($i == $page) {
			$html .= "<strong>$i</strong>&#32;";
		}
		else {
			$link_i = page_link($i);
			$html .= "<a href=\"$link_i\">$i</a>&#32;";
		}
		if ($i >= $total_pages) {
			break;
		}
	}
	if ($page < $total_pages) {
		$link_next = page_link($page + 1);
		$link_last = page_link($total_pages);
		$html .= "<a href=\"$link_next\">{$labels['next']}</a>&#32;";
		$html .= "<a href=\"$link_last\">{$labels['last']}</a>";
	}
	$html .= "</div>";
	return $html;
}

?>


</body>
</html>
